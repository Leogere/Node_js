var http = require('http') ; // require effectue un appel à une bibliothèque de Node.js, ici la bibliothèque "http" qui nous permet de créer un serveur web
var url = require('url');
var querystring = require('querystring') ; //Découpe au préalable les différents paramètres
var server = http.createServer(function(req, res){ 
    var params = querystring.parse(url.parse(req.url).query) 
    res.writeHead(200 ,{"Content-Type": "text/plain"}); 
    if('prenom' in params && 'nom' in params){
        res.write("Vous vous appelez " + params['prenom'] + " " + params['nom']) ;
    }
    else{
        res.write("Vous n'avez pas de nom ?")
    }
 
    res.end();
});
server.listen(8080);// le serveur est lancé et "écoute" sur le port 8080 